---
layout: job_family_page
title: "People Systems and Analytics"
---

The People Systems and Analytics Job Family owns the technology and reporting solutions at Gitlab. Working in partnership with the People Leadership team, they play both a strategic and tactical role setting the direction for the technology and analytics strategy at Gitlab and operating with a “data-first” mindset.

## Levels

### Senior Systems and Analytics Partner

The Senior Systems and Analytics Partner reports into the [Director, People Tools and Technology](/job-families/people-ops/leadership-people-operations-tech-analytics/#director-people-tools-and-technology).

#### Senior Systems and Analytics Partner Job Grade

The Senior Systems and Analytics Partner is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Systems and Analytics Partner Responsibilities

* Own technology, reporting and insights and Gitlab
* Develop consultative business partnerships with People Leadership to address challenges and influence business decisions through insights.
* Lead multiple systems and analytics projects at a time, from conception to completion.
* Manage the proactive and reactive split for data needs. Ensure a relevant suite of deliverables for ongoing requirements while also supporting adhoc requests.
* Drive elements of the Workday and Greenhouse Integration to improve recruiter workflows, daily roadblocks, and interruptions and design processes to eliminate data inconsistencies.
* Empower leaders and organizations by providing high quality and timely reporting, analyses and actionable insights.
* In partnership with a centralized Data team, design, develop and automate self-service dashboards to ensure our business leadership have easy access to the right information when monitoring organizational health and making talent decisions.
* Develop new models and methods for evaluating existing data or correlating new to existing data sources for intuitive reporting solutions.
* Clean, organize and synthesize large data sets and determine actions and insights based on findings.  
* Handle large amounts of sensitive and confidential data with discretion and care.  
* Manage vendor relationships for the recruiting team.

#### Senior Systems and Analytics Partner Requirements

* Previous recruiting market/talent intelligence or talent mapping experience
* Consulting skills with the demonstrated ability to successfully influence clients at all levels.
* Experience building reports and dashboards in a data visualization tool
* Experience with a variety of data sources
* Experience in a remote startup environment
* Proven ability to multitask and prioritize workload
* Excellent communication and interpersonal skills
* Demonstrated ability to work in a team environment and work collaboratively
* Proficient in Google Workspace
* Willingness to learn and use software tools including Git and GitLab, prior experience with GitLab is a plus.
* Ability to recognize and appropriately handle highly sensitive and privileged information
* Prior experience using an applicant tracking system (ATS), such as Greenhouse
* Prior experience using external recruiting tools such as LinkedIn Talent Insights
* Prior experience using a human resources information system (HRIS), such as Workday, is a plus
* Share our values and work in accordance with those value

## Specialties 

#### Systems and Analytics Partner (Talent Acquisition) Responsibilities

* Extends that of the Senior Systems and Analytics Partner responsibilities
* Partner with the GitLab Global Talent Acquisition team
* Develop consultative business partnerships with TA Leadership to address challenges and influence business decisions through insights.

#### Systems and Analytics Partner (Talent Acquisition) Requirements

* Extends that of the Senior Systems and Analytics Partner requirements
* Experience working directly with Talent Acquistion functions

#### Systems and Analytics Partner (Talent Acquisition) Performance Indicators

* Active Users with a Recruiting or Hiring Manager LinkedIn Seat
* Average Location Factor
* Cost Per Hire
* Hires vs Plan
* Time to Offer Accept

## Performance Indicators

Overall job family performance indicators are yet to be determined.

## Career Ladder

The next step in the People Systems and Analytics Job Family has not yet been defined.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Selected candidates will be invited to schedule a 30min screening call with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 team members
* There may be a final executive interview

As always, the interviews and screening call will be conducted via a [video call](/handbook/communication/#video-calls). See more details about our interview process [here](/handbook/hiring/interviewing/).
