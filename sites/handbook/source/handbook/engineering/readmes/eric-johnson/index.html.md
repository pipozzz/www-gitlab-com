---
layout: markdown_page
title: "Eric Johnson's README"
job: "CTO"
---

This page was inspired by the recent trend of Engineering Manager README's. _e.g._ [Hackernoon: 12 Manager READMEs (from some of the best cultures in tech)](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe).

## Eric J's README

My name is Eric Johnson and I'm the Chief Technology Officer at GitLab.

* [GitLab Handle](https://gitlab.com/edjdev)
* [Team Page](/company/team/#edjdev)

Here are some things about me:

* I'm the veteran of 5 startups (including GitLab). I've only ever worked for startups. I'm geared for growth.
* I like learning. My previous startups have been in marketing technology, localization software, video streaming, and aerospace.
* I believe in the [servant-leader](https://en.wikipedia.org/wiki/Servant_leadership) model. Management is a specialty like Frontend, Backend, Security, or DevOps. And that means aptitude comes with an imperative to make others' around you better by this skill. There is no prestige in reporting lines: your manager should be whomever makes you most effective, regardless of level.
* Emotionally, I never get too high, or too low. A strength is I never have 'hair-on-fire' moments. A weakness is that I could be better at the inspirational style of motivation that comes from stirring speeches, and whatnot. I commonly hear that I am 'difficult to read'. You're likely overthinking it: just take me at face value.
* I value communication more than most engineering leaders. It's the mortar between the bricks that allows us to build massive projects
* My email inbox is my to do list. Everything gets read, actioned, answered, and archived. 100% SLA.
* I always keep my promises. And I greatly value people that do the same. I sometimes wonder if those who gracefully let the tail-end of their backlog slip through the cracks are more effective and efficient, but I can't bring myself to allow that to happen
* I come from a non-traditional engineering background. I studied Philosophy in college and programmed on the side. Nevertheless, I went back and painted in my skill set with computer science fundamentals. I recommend that all self-taught programmers do the same.
* In addition to engineering teams I've also managed designers, localization professionals, and support staff
* Besides the project and company we're creating, I think it's important to society that we figure out how remote-only work can scale because it's better for people and the environment
* Here is [my 1:1 meeting template](https://docs.google.com/document/d/1vWm7-lmpqghoElckd02puqsKDNT6aCJInuZcfkdtvwQ/edit)
  * Populate the agenda the day before (I set a 1 day email reminder for myself in my calendar)
  * Copy down the orange template and set text to black
  * Reports should drive 80% of agenda to allow me to scale (their reports should do the same for them)
  * Reports have edit rights to the 1:1 calendar invite and can just move it to a mutually free time
  * Use Google doc action items
  * “Priorities” is to list your top priorities. We can re-order and establish a cut-line together
  * Feedback is for real-time, bi-directional performance feedback (_e.g._ good, bad, try)
  * Let's memorialize decisions in-line
* Outside of work I love travel, solving puzzles with code, and spending time with my family.
* I type too fast and make spelling mistakes where there isn't a native spell checker. I'm working on it. 😁

Do you know something about me that belongs here? [submit an MR to this page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/handbook/engineering/erics-readme/index.html.md).

## My Remote Work Setup

### Camera

* [Sony a6400 Mirrorless Camera](https://www.sony.com/electronics/interchangeable-lens-cameras/ilce-6400)
* [Sony 35mm F1.8 OSS Prime Lens](https://www.sony.com/electronics/camera-lenses/sel35f18)
* [TKDY Dummy Battery](https://www.amazon.com/gp/product/B07144X7MC/) (to power the camera)
* [6ft Micro HDMI to HDMI cable](https://www.amazon.com/gp/product/B00Z07JYLE/)
* [Elgato Camlink 4K](https://www.elgato.com/en/gaming/cam-link-4k)<sup>1</sup>
* [Elgato Multi Mount](https://www.elgato.com/en/master-mount)
* [Elgato Flex Arm](https://www.elgato.com/en/flex-arm)

<sup>1</sup> Eventually Sony should release their free [Imaging Edge Webcam](https://support.d-imaging.sony.co.jp/app/webcam/en/download/) software for MacOS, which will eliminate the need for the Camlink 4K.

### Lighting

* 2x [Elgato Key Lights](https://www.elgato.com/en/gaming/key-light) (as the "Key" and "Fill" lights in the [Three Pointing Lighting](https://en.wikipedia.org/wiki/Three-point_lighting) method)

### Microphone

* [Elgato Wave:3 Microphone](https://www.elgato.com/en/wave-3) with their WaveLink Digital Mixer Software
* [Elgato Wave Pop Filter](https://www.elgato.com/en/wave-pop-filter)
* [Elgato Wave Shock Mount](https://www.elgato.com/en/wave-shock-mount)
* [VIVO Pneumatic Mic Arm](https://vivo-us.com/products/stand-mic01)

### Other

* Apple iMac (Retina 5K, 27-inch, 2017)
* [ECamm Live](https://www.ecamm.com/) Software (for picture-in-picture screen sharing)<sup>2</sup>
* [Elgato Stream Deck XL](https://www.elgato.com/en/gaming/stream-deck-xl) (to control the lights, Ecamm Scenes, and type words I frequently mispell)
* [Google Pixelbook Go](https://store.google.com/us/product/pixelbook_go) (for travel)
* [Vari Electric Standing Desk](https://www.vari.com/electric-standing-desk-60x30/FD-ESD6030.html?dwvar_FD-ESD6030_color=reclaimed-wood)
* [Vari Active Seat](https://www.vari.com/active-seat/ST-ACTV.html)

<sup>2</sup> Eventually [OBS (Open Broadcaster Software)](https://obsproject.com/) should release their own virtual camera for MacOS and I'll consider switching from Ecamm Live.
